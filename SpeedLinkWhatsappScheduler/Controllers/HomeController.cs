﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpeedLinkWhatsappScheduler.Models;
using SpeedLinkWhatsappScheduler.Services;

//inititates the timer singleton
namespace SpeedLinkWhatsappScheduler.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {

        private readonly ITimer timer;

        public HomeController(ITimer timer)
        {
            this.timer = timer;
        }

        [Route("")]
        public IActionResult Index()
        {
            return Ok("timer is running");
        }
    }
}
