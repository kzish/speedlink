﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WalletApi.Controllers
{
    /// <summary>
    /// this is a proxy class that will send messages on the selected platforms
    /// </summary>
    public class WhatsAppWalletApiController : Controller
    {
        private static int platform = Globals.Platform;

        //receive message callback and executes the menu
        public static void RecieveMessageCallBack(string to, string message)
        {

            WhatsAppWalletApiController
                .SendMessageText(to, message).Wait();

        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageText(mobileNumber, message_text);
            }
            if (platform == 2)
            {
                message_text = message_text.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageText(mobileNumber, message_text);
            }
            return new JsonResult(new { });
        }
         
        //send a text message template
        public static async Task<JsonResult> SendMessageTextTemplate(string mobileNumber,string template_message)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageText(mobileNumber,template_message);
            }
            if (platform == 2)
            {
                await GupshupWalletApiController.SendMessageTextTemplate(mobileNumber,template_message);
            }
            return new JsonResult(new { });
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content, string caption)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageImageAndText(mobileNumber, image_content, caption);
            }
            if (platform == 2)
            {
                caption = caption.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageImageAndText(mobileNumber, image_content, HttpUtility.HtmlEncode(caption));
            }
            return new JsonResult(new { });
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            //if (platform == 1) await TelegramWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //if (platform == 2) await GupshupWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //send errot through telegram always
            await TelegramWalletApiController.SendErrorMessageText("586097924","speedlink whatsapp scheduler:"+ message_text);
            return new JsonResult(new { });
        }




    }
}
