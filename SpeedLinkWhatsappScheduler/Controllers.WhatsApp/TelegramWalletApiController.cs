﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class TelegramWalletApiController : Controller
    {

        //speedlink rubiem_bot speedlink
        private static string telegram_access_token = "1285353088:AAHIkUv-QXGQBEgFNLpKenb-CtRZiawB-pQ";
        private static TelegramBotClient botClient;

        //constructor
        public TelegramWalletApiController()
        {
            if (botClient == null)
            {
                botClient = new TelegramBotClient(telegram_access_token);
            }
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (botClient == null)
            {
                botClient = new TelegramBotClient(telegram_access_token);
            }
            try
            {
                Message message = await botClient
                    .SendTextMessageAsync(
                      chatId: mobileNumber,
                      text: message_text,
                      parseMode: ParseMode.Html,
                      disableNotification: false
                  );
                return new JsonResult(new
                {
                    res = "ok",
                    msg = message
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content, string caption)
        {
            if (botClient == null)
            {
                botClient = new TelegramBotClient(telegram_access_token);
            }
            try
            {
                Message message = await botClient.SendPhotoAsync(
                    chatId: mobileNumber,
                    photo: "https://www.wcaworld.com/logos/69726.JPG",//"https://github.com/TelegramBots/book/raw/master/src/docs/photo-ara.jpg",
                    caption: caption,
                    parseMode: ParseMode.Html
                );
                return new JsonResult(new { res = "err", msg = message });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { res = "err", msg = ex.Message });
            }
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber,message_text);
        }

    }
}
