﻿using System;
using System.Collections.Generic;

namespace SpeedLinkWhatsappScheduler.Models
{
    public partial class MSubscriptions
    {
        public int Id { get; set; }
        public string AspNetUserId { get; set; }
        public string WhatsappMobileNumber { get; set; }
        public string JobNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string JobDescription { get; set; }

        public virtual AspNetUsers AspNetUser { get; set; }
    }
}
