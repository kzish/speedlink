﻿using System;
using System.Collections.Generic;

namespace SpeedLinkWhatsappScheduler.Models
{
    public partial class MChatMessage
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string CustomerNumber { get; set; }
        public string Sender { get; set; }
        public string Reciever { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
    }
}
