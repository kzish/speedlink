﻿using System;
using System.Collections.Generic;

namespace SpeedLinkWhatsappScheduler.Models
{
    public partial class MScheduledUpdates
    {
        public int Id { get; set; }
        public string AspNetUserId { get; set; }
        public bool Whatsapp { get; set; }
        public bool Sms { get; set; }
        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public TimeSpan SendTime { get; set; }
        public DateTime DateLastSent { get; set; }

        public virtual AspNetUsers AspNetUser { get; set; }
    }
}
