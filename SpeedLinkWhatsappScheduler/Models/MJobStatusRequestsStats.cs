﻿using System;
using System.Collections.Generic;

namespace SpeedLinkWhatsappScheduler.Models
{
    public partial class MJobStatusRequestsStats
    {
        public int Id { get; set; }
        public string AspNetUserId { get; set; }
        public DateTime Date { get; set; }

        public virtual AspNetUsers AspNetUser { get; set; }
    }
}
