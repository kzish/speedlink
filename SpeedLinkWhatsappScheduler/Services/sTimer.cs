﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Http;
using SpeedLinkWhatsappScheduler.Models;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using WalletApi.Controllers;
using System.ServiceModel;

namespace SpeedLinkWhatsappScheduler.Services
{
    public class sTimer : ITimer
    {

        private Timer timer;
        private bool busy;
        //private dbContext db;

        //constructor
        public sTimer()
        {
            //db = new dbContext();
            Globals.log_data_to_file("runtask started");
            var autoEvent = new AutoResetEvent(false);
            timer = new Timer(RunTask, autoEvent, 0, 5000);//5 sec timer
        }

        //task will send the message
        public void RunTask(object state)
        {
            //Globals.log_data_to_file("runtask");
            if (busy)
            {
                return;
            }

            try
            {
                busy = true;
                //get list of the schedules in the database that are ready to send

                var time = DateTime.Now.TimeOfDay;//current time
                var day = DateTime.Now.DayOfWeek.ToString().ToLower().Substring(0, 3);//current day of the week

                using (var db = new dbContext())
                {
                    var schedules = db.MScheduledUpdates.AsQueryable();
                    if (day == "sun") schedules = schedules.Where(i => i.Sun);
                    if (day == "mon") schedules = schedules.Where(i => i.Mon);
                    if (day == "tue") schedules = schedules.Where(i => i.Tue);
                    if (day == "wed") schedules = schedules.Where(i => i.Wed);
                    if (day == "thu") schedules = schedules.Where(i => i.Thu);
                    if (day == "fri") schedules = schedules.Where(i => i.Fri);
                    if (day == "sat") schedules = schedules.Where(i => i.Sat);

                    //schedules where sendtime is elapsed since current time but have not been sent today(DateTime.Now.Date)
                    //also filter whatsapp and sms
                    schedules = schedules.Where(
                            i =>

                            i.SendTime <= time //send time less than or equal to now, meaning the time to send has lapsed


                            && i.DateLastSent != DateTime.Now.Date//todays schedule not sent
                            && i.Whatsapp//is whats app enabled
                        );


                    var selected_schedules = schedules.ToList();
                    bool there_are_changes = false;
                    foreach (var s in selected_schedules)
                    {
                        //send the notification and update the DateLastSent
                        SendWhatsappUpdate(s).Wait();//send async and dont wait
                        s.DateLastSent = DateTime.Now;//update last sent date
                        there_are_changes = true;
                    }
                    if (there_are_changes) db.SaveChanges();
                    db.Dispose();
                }//using

            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs() { Data1 = "SpeedLinkWhatsappScheduler.Services.sTimer.RunTask: " + ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    db.Logs.Add(log);
                    db.SaveChanges();
                    db.Dispose();
                }
            }
            finally
            {
                busy = false;
            }


        }//runtask


        /// <summary>
        /// send the update to the client
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        private async Task<bool> SendWhatsappUpdate(MScheduledUpdates schedule)
        {
            var client = GetClient();
            try
            {
                List<MSubscriptions> subs = null;
                using (var db = new dbContext())
                {
                    subs = db.MSubscriptions.Where(i => i.AspNetUserId == schedule.AspNetUserId).ToList();
                    db.Dispose();
                }
                string statuses = string.Empty;
                string mobile = subs.First().WhatsappMobileNumber;
                foreach (var sub in subs)
                {
                    var jobUpdates = new WS.Response();
                    var request = new WS.ManageJobStatusUpdateRequest(jobUpdates, sub.CustomerNumber, sub.JobNumber);
                    var job_status_request = client.ManageJobStatusUpdateRequestAsync(request).Result;
                    if (job_status_request.return_value)
                    {
                        var customerOrderNo = job_status_request.jobUpdates.JobItems[0].CustomerOrderNo[0];
                        var statusUpdate = job_status_request.jobUpdates.JobItems[0].UpdateList[0].Status[0];
                        statuses +=  $" *{customerOrderNo.Trim()}* :{statusUpdate.Trim()}";
                    }
                }//foreach
                statuses = statuses
                    .Replace("\n", " ")
                    .Replace("\r", "");

                var gupshup_template = "The status of {{1}} has been updated to {{2}}.";//gupshup template 25
                var msg = gupshup_template
                    .Replace("{{1}}", $"Jobs")
                    .Replace("{{2}}", $"{statuses}");
                //send each update in a template formate
                await WhatsAppWalletApiController.SendMessageTextTemplate(mobile, msg);
                return true;
            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs() { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    db.Logs.Add(log);
                    db.SaveChanges();
                    db.Dispose();
                }
                return false;
            }
            finally
            {
                client.CloseAsync().Wait();
            }

        }

        private static WS.AppIntegrationMgt_PortClient GetClient()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            //configure end point
            var endPoint = new EndpointAddress("http://196.44.191.67:7047/AppIntegration/WS/Speedlink%20Cargo%20Pvt%20Ltd/Codeunit/AppIntegrationMgt");
            //declare client
            WS.AppIntegrationMgt_PortClient sc_client = new WS.AppIntegrationMgt_PortClient(myBinding, endPoint);
            //sc_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("Webuser", "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=");
            sc_client.ClientCredentials.UserName.UserName = "Webuser";
            sc_client.ClientCredentials.UserName.Password = "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=";
            return sc_client;
        }


    }//sTimer







}//namespace
