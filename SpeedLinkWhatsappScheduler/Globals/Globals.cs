﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Globals
{

    //live 
    public static string gupshup_end_point = "https://api.gupshup.io/sm/api/v1/msg";
    public static string gupshup_apikey = "3cab8b6156a4454ec5a6cbd6b00d528c";
    public static string gupshup_source = "263773329403";
    public static string src_name = "Speedlinkcargo";

    private static string log_file=@"C:\rubiem\speedlink.log";
    public static string speedlink_api_end_point= "https://rubieminnovations.com:9002/SpeedLinkApi/speedlink/v1";//online
    //public static string speedlink_api_end_point = "https://localhost:44341/speedlink/v1";//offline

    public static string oauth_end_point = $"{speedlink_api_end_point}/Auth/RequestToken?clientID=test_user&clientSecret=12345";

    public static void log_data_to_file(string data)
    {
        try
        {
            System.IO.File.AppendAllText(log_file, data + Environment.NewLine + Environment.NewLine);
        }catch(Exception ex)
        {

        }
    }

    //for local
    public const string DbContextConnectionString =
        "User ID=root;Password=password;Server=TERRENCE\\SQLEXPRESS;Initial Catalog=speedlink;";

    //for production
//    public const string DbContextConnectionString =
//        @"server=.\SQLEXPRESS;database=speedlink;User Id=sa;Password=gZi3hg8dn210Q;";

    //1=telegram
    //2=gupshup
    public const int Platform = 1;
}
