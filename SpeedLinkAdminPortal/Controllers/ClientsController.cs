﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpeedLinkAdminPortal.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace Admin.Controllers
{
    [Route("Clients")]
    //[Authorize(Roles = "admin")]
    public class ClientsController : Controller
    {
        dbContext db = new dbContext();
        private string source = "Admin.Controllers.ClientsController.";
        UserManager<IdentityUser> userManager;
        RoleManager<IdentityRole> roleManager;

        public ClientsController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [HttpGet("ajaxListClients")]
        public IActionResult ajaxListClients()
        {
            ViewBag.title = "Clients";
            var clients = db.MClient.Include(i=>i.IdNavigation).ToList();
            ViewBag.clients = clients;
            return View();
        }

        [HttpGet("Index")]
        [HttpGet("")]
        public IActionResult Index()
        {
            ViewBag.title = "Clients";
            return View();
        }

        [HttpGet("DeleteClient/{id}")]
        public async Task<IActionResult> DeleteClient(string id)
        {
            ViewBag.title = "Delete Client";
            //delete user which also deletes client
            try
            {
                var client_user = await userManager.FindByIdAsync(id);
                var client = db.MClient.Where(i => i.Id == id).FirstOrDefault();
                db.Remove(client);
                db.SaveChanges();
                await userManager.DeleteAsync(client_user);
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeleteClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction("Index");
            }
        }


        [HttpGet("CreateClient")]
        public IActionResult CreateClient()
        {
            ViewBag.title = "Create Client";
            return View();
        }

        [HttpPost("CreateClient")]
        public async Task<IActionResult> CreateClient(string Email, string Password, MClient client)
        {
            ViewBag.title = "Create Client";
            try
            {
                //create a user and create  client, then link the client to the user
                //assign client to client roles
                var id_user = new IdentityUser()
                {
                    Email = Email,
                    PasswordHash = Password,
                    UserName = Email
                };
                var created = await userManager.CreateAsync(id_user, Password);
                if (created.Succeeded)
                {
                    client.Id = id_user.Id;//link to the user
                    db.MClient.Add(client);
                    //create role if not exists
                    var client_role = await roleManager.RoleExistsAsync("client");
                    if (!client_role)
                    {
                        await roleManager.CreateAsync(new IdentityRole("client"));
                    }
                    //add client to role
                    var is_in_role = await userManager.IsInRoleAsync(id_user, "client");
                    if (!is_in_role)
                    {
                        await userManager.AddToRoleAsync(id_user, "client");
                    }
                    TempData["type"] = "success";
                    TempData["msg"] = "Saved";
                }
                else
                {
                    TempData["type"] = "error";
                    var errors = string.Empty;
                    foreach (var e in created.Errors.ToList())
                    {
                        errors += e.Description + " ";
                    }
                    TempData["msg"] = errors;
                }

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "CreateClient", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error: " + ex.Message;
                return RedirectToAction($"CreateClient", "Clients");
            }
        }
        [HttpGet("EditClient/{id}")]
        public IActionResult EditClient(string id)
        {
            ViewBag.title = "Edit Client";
            var client = db.AspNetUsers.Where(i => i.Id == id).Include(i => i.MClient).FirstOrDefault();
            ViewBag.client = client;
            return View();
        }
        [HttpPost("EditClient")]
        public IActionResult EditClient(MClient client, string id)
        {
            ViewBag.title = "Edit Client";
            try
            {
                var asp_net_user = db.AspNetUsers.Where(i => i.Id == id).Include(i => i.MClient).FirstOrDefault();
                asp_net_user.MClient = client;
                db.SaveChanges();
                TempData["msg"] = "Saved";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["msg"] = ex.Message;
                TempData["type"] = "error";
            }
            return RedirectToAction("EditClient", new { id });
        }

        [HttpGet("ScheduleClientUpdates/{id}")]
        public IActionResult ScheduleClientUpdates(string id)
        {
            var asp_net_user = db.AspNetUsers
                .Where(i => i.Id == id)
                .Include(i => i.MScheduledUpdates)
                .Include(i => i.MClient)
                .FirstOrDefault();
            ViewBag.title = "ScheduleClientUpdates";
            ViewBag.client = asp_net_user;
            return View();
        }

        [HttpPost("AddScheduleClientUpdates")]
        public IActionResult AddScheduleClientUpdates(MScheduledUpdates schedule)
        {
            try
            {
                ViewBag.title = "ScheduleClientUpdates";
                db.MScheduledUpdates.Add(schedule);
                db.SaveChanges();
                TempData["msg"] = "Saved";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["msg"] = ex.Message;
                TempData["type"] = "error";
            }
            return RedirectToAction("ScheduleClientUpdates",new { id = schedule.AspNetUserId });
        }

        [HttpGet("DeleteScheduleClientUpdates/{id}")]
        public IActionResult DeleteScheduleClientUpdates(int id)
        {
            string asp_net_user_id=string.Empty;
            try
            {
                ViewBag.title = "DeleteScheduleClientUpdates";
                var schedule = db.MScheduledUpdates.Find(id);
                asp_net_user_id = schedule.AspNetUserId;
                db.MScheduledUpdates.Remove(schedule);
                db.SaveChanges();
                TempData["msg"] = "Deleted";
                TempData["type"] = "success";
            }
            catch (Exception ex)
            {
                TempData["msg"] = ex.Message;
                TempData["type"] = "error";
            }
            return RedirectToAction("ScheduleClientUpdates", new { id= asp_net_user_id});
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


    }
}
