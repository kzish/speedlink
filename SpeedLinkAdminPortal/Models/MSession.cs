﻿using System;
using System.Collections.Generic;

namespace SpeedLinkAdminPortal.Models
{
    public partial class MSession
    {
        public string WhatsappMobileNumber { get; set; }
        public DateTime? Date { get; set; }
        public string CustomerNumber { get; set; }
        public string LastMenu { get; set; }
        public string LastResponse { get; set; }
        public string SpeedlinkName { get; set; }
        public int NextCount { get; set; }
        public int PrevCount { get; set; }
    }
}
