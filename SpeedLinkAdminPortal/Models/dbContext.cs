﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SpeedLinkAdminPortal.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<MChatMessage> MChatMessage { get; set; }
        public virtual DbSet<MClient> MClient { get; set; }
        public virtual DbSet<MJobStatusRequestsStats> MJobStatusRequestsStats { get; set; }
        public virtual DbSet<MScheduledUpdates> MScheduledUpdates { get; set; }
        public virtual DbSet<MSession> MSession { get; set; }
        public virtual DbSet<MSubscriptions> MSubscriptions { get; set; }
        public virtual DbSet<MWhatsappUsageStats> MWhatsappUsageStats { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=TERRENCE\\SQLEXPRESS;database=speedlink;User Id=root;Password=password;");
                //optionsBuilder.UseSqlServer(@"server=WIN-VJK2LL0LG4U\SQLEXPRESS;database=speedlink;User Id=sa;Password=gZi3hg8dn210Q;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<MChatMessage>(entity =>
            {
                entity.ToTable("m_chat_message");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerNumber)
                    .IsRequired()
                    .HasColumnName("customer_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsRead).HasColumnName("is_read");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .IsUnicode(false);

                entity.Property(e => e.Reciever)
                    .IsRequired()
                    .HasColumnName("reciever")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sender)
                    .IsRequired()
                    .HasColumnName("sender")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MClient>(entity =>
            {
                entity.ToTable("m_client");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CustomerNumber)
                    .HasColumnName("customer_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MobileSms)
                    .HasColumnName("mobile_sms")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MobileWhatsapp)
                    .HasColumnName("mobile_whatsapp")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.MClient)
                    .HasForeignKey<MClient>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_client_AspNetUsers");
            });

            modelBuilder.Entity<MJobStatusRequestsStats>(entity =>
            {
                entity.ToTable("m_job_status_requests_stats");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AspNetUserId)
                    .IsRequired()
                    .HasColumnName("asp_net_user_id")
                    .HasMaxLength(450);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.AspNetUser)
                    .WithMany(p => p.MJobStatusRequestsStats)
                    .HasForeignKey(d => d.AspNetUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_job_status_requests_stats_AspNetUsers");
            });

            modelBuilder.Entity<MScheduledUpdates>(entity =>
            {
                entity.ToTable("m_scheduled_updates");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AspNetUserId)
                    .IsRequired()
                    .HasColumnName("asp_net_user_id")
                    .HasMaxLength(450);

                entity.Property(e => e.DateLastSent)
                    .HasColumnName("date_last_sent")
                    .HasColumnType("date");

                entity.Property(e => e.Fri).HasColumnName("fri");

                entity.Property(e => e.Mon).HasColumnName("mon");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.SendTime).HasColumnName("send_time");

                entity.Property(e => e.Sms).HasColumnName("sms");

                entity.Property(e => e.Sun).HasColumnName("sun");

                entity.Property(e => e.Thu).HasColumnName("thu");

                entity.Property(e => e.Tue).HasColumnName("tue");

                entity.Property(e => e.Wed).HasColumnName("wed");

                entity.Property(e => e.Whatsapp).HasColumnName("whatsapp");

                entity.HasOne(d => d.AspNetUser)
                    .WithMany(p => p.MScheduledUpdates)
                    .HasForeignKey(d => d.AspNetUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_scheduled_updates_AspNetUsers");
            });

            modelBuilder.Entity<MSession>(entity =>
            {
                entity.HasKey(e => e.WhatsappMobileNumber);

                entity.ToTable("m_session");

                entity.Property(e => e.WhatsappMobileNumber)
                    .HasColumnName("whatsapp_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CustomerNumber)
                    .HasColumnName("customer_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastMenu)
                    .HasColumnName("last_menu")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastResponse)
                    .HasColumnName("last_response")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NextCount).HasColumnName("next_count");

                entity.Property(e => e.PrevCount).HasColumnName("prev_count");

                entity.Property(e => e.SpeedlinkName)
                    .HasColumnName("speedlink_name")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MSubscriptions>(entity =>
            {
                entity.ToTable("m_subscriptions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AspNetUserId)
                    .HasColumnName("asp_net_user_id")
                    .HasMaxLength(450);

                entity.Property(e => e.CustomerNumber)
                    .HasColumnName("customer_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobDescription)
                    .HasColumnName("job_description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.JobNumber)
                    .HasColumnName("job_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WhatsappMobileNumber)
                    .HasColumnName("whatsapp_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AspNetUser)
                    .WithMany(p => p.MSubscriptions)
                    .HasForeignKey(d => d.AspNetUserId)
                    .HasConstraintName("FK_m_subscriptions_AspNetUsers");
            });

            modelBuilder.Entity<MWhatsappUsageStats>(entity =>
            {
                entity.ToTable("m_whatsapp_usage_stats");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AspNetUserId)
                    .IsRequired()
                    .HasColumnName("asp_net_user_id")
                    .HasMaxLength(450);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.AspNetUser)
                    .WithMany(p => p.MWhatsappUsageStats)
                    .HasForeignKey(d => d.AspNetUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_m_whatsapp_usage_stats_AspNetUsers");
            });
        }
    }
}
