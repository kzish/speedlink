﻿using System;
using System.Collections.Generic;

namespace SpeedLinkAdminPortal.Models
{
    public partial class WhatsAppMenusOptions
    {
        public string MenuId { get; set; }
        public string OptionId { get; set; }
        public string OptionText { get; set; }
        public string ParentMenuId { get; set; }
        public string ChildMenuId { get; set; }
        public string Action { get; set; }

        public virtual WhatsAppMenus Menu { get; set; }
    }
}
