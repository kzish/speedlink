﻿using System;
using System.Collections.Generic;

namespace SpeedLinkAdminPortal.Models
{
    public partial class MClient
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string MobileSms { get; set; }
        public string MobileWhatsapp { get; set; }
        public string CustomerNumber { get; set; }

        public virtual AspNetUsers IdNavigation { get; set; }
    }
}
