﻿using System;
using System.Collections.Generic;

namespace SpeedLinkAdminPortal.Models
{
    public partial class WhatsAppMenus
    {
        public WhatsAppMenus()
        {
            WhatsAppMenusOptions = new HashSet<WhatsAppMenusOptions>();
        }

        public string MenuId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }

        public virtual ICollection<WhatsAppMenusOptions> WhatsAppMenusOptions { get; set; }
    }
}
