﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpeedLinkApi.Models;


namespace SpeedLinkApi.Controllers
{
    [AllowAnonymous]
    [Route("speedlink/v1")]
    //[EnableCors]
    public class ChatController : Controller
    {
        private dbContext db = new dbContext();
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        /// <summary>
        /// send the message to the client and admin
        /// persist a message into the db
        /// used by admin_vue_client only
        /// </summary>
        /// <param name="message_"></param>
        /// <returns></returns>
        [HttpPost("SaveChatMessage")]
        public async Task<JsonResult> SaveChatMessage([FromBody]MChatMessage message_)
        {
            try
            {

                if (MQTT.mqtt == null || !MQTT.mqtt.IsConnected)
                {
                    MQTT.initMqttClient();//check init and connect
                }

                message_.Date = DateTime.Now;
                message_.IsRead = true;
                db.MChatMessage.Add(message_);
                await db.SaveChangesAsync();

                //send a message to the reciever and cc the admin_vue_client
                string json_message = JsonConvert.SerializeObject(message_);
                MQTT.mqtt.Publish(message_.CustomerNumber, Encoding.ASCII.GetBytes(json_message), 1, false);
                //
                dynamic json = new JObject();
                json.Sender = message_.Sender;
                json.Message = message_.Message;
                json.CustomerNumber = message_.CustomerNumber;
                json.Date = DateTime.Now;
                MQTT.mqtt.Publish("admin_client", Encoding.ASCII.GetBytes(json.ToString()), 1, false);


               
                return Json(new
                {
                    res = "ok",
                    data = "Message Saved"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    data = ex.Message
                });
            }
        }


        /// <summary>
        /// fetch all the past messages with this chat
        /// </summary>
        /// <param name="asp_net_user_id"></param>
        /// <returns></returns>
        [HttpGet("GetMessages")]
        public async Task<JsonResult> GetMessages(string CustomerNumber)
        {
            try
            {
                var messages = db.MChatMessage
                    .Where(i => i.CustomerNumber == CustomerNumber)
                    .OrderBy(i => i.Date)
                    .ToList();
                foreach(var msg in messages)
                {
                    msg.IsRead = true;//mark all these messages as read
                }
                await db.SaveChangesAsync();
                return Json(new
                {
                    res = "ok",
                    data = messages
                }, new Newtonsoft.Json.JsonSerializerSettings()
                {
                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// fetch all my contacts and the last message sent
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetContacts")]
        public JsonResult GetContacts()
        {
            try
            {
                

                var contacts = new JArray();
                var clients = db.MClient.ToList();
                foreach(var c in clients)
                {
                    var has_un_read_message = db.MChatMessage
                        .Where(i => !i.IsRead && i.CustomerNumber == c.CustomerNumber)
                        .Any();
                    dynamic obj = new JObject();
                    obj.CustomerNumber = c.CustomerNumber;
                    obj.Name = c.Name;
                    obj.HasUnReadMessages = has_un_read_message;
                    contacts.Add(obj);
                }
                return Json(new
                {
                    res = "ok",
                    data = contacts
                }, new Newtonsoft.Json.JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                    Formatting = Newtonsoft.Json.Formatting.Indented,
                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        [HttpPost("MarkAsRead")]
        public async Task<JsonResult> MarkAsRead(int message_id)
        {
            try
            {
                var message = db.MChatMessage.Find(message_id);
                message.IsRead = true;
                await db.SaveChangesAsync();
                return Json(new
                {
                    res = "ok",
                    msg = "saved"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


    }
}
