﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpeedLinkApi.Models;
using WS;

namespace SpeedLinkApi.Controllers
{
    [Route("speedlink/v1")]
    //[Authorize]
    [Authorize(AuthenticationSchemes = "Bearer")] //allow only authorized by Bearer
    public class ProcessController : Controller
    {
        private readonly dbContext _db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _db.Dispose();
        }


        /// <summary>
        /// return the soap client
        /// </summary>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        private static AppIntegrationMgt_PortClient GetClient()
        {
            //configure binding
            var binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.MaxReceivedMessageSize = 20000000;
            binding.MaxBufferSize = 20000000;
            binding.MaxBufferPoolSize = 20000000;
            binding.AllowCookies = true;
            binding.ReaderQuotas = new XmlDictionaryReaderQuotas
            {
                MaxArrayLength = 20000000,
                MaxStringContentLength = 20000000,
                MaxDepth = 32
            };

            //configure end point
            var endPoint =
                new EndpointAddress(
                    "http://196.44.191.67:7047/AppIntegration/WS/Speedlink%20Cargo%20Pvt%20Ltd/Codeunit/AppIntegrationMgt");
            //declare client
            var scClient = new AppIntegrationMgt_PortClient(binding, endPoint);
            //sc_client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("Webuser", "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=");
            scClient.ClientCredentials.UserName.UserName = "Webuser";
            scClient.ClientCredentials.UserName.Password = "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=";
            return scClient;
        }

        /// <summary>
        /// status of a specific job
        /// </summary>
        /// <param name="customerNo"></param>
        /// <param name="jobNo"></param>
        /// <returns></returns>
        [HttpPost("JobStatusRequest")]
        public async Task<JsonResult> ManageJobStatusUpdateRequest(string customerNo, string jobNo)
        {
            if (jobNo.Length >= 11) jobNo = jobNo.Substring(0, 10); //take only the first ten chars
            try
            {
                var client = GetClient();
                var jobUpdates = new Response();
                var request = new ManageJobStatusUpdateRequest(jobUpdates, customerNo, jobNo);
                var response = await client.ManageJobStatusUpdateRequestAsync(request);
                if (!response.return_value)
                    return Json(new
                    {
                        res = "err",
                        msg = response.jobUpdates.ResponseMessage[0],
                    });
                //save stats
                var mClient = _db.MClient.FirstOrDefault(i => i.CustomerNumber == customerNo);
                var usage = new MJobStatusRequestsStats {AspNetUserId = mClient?.Id, Date = DateTime.Now};
                await _db.MJobStatusRequestsStats.AddAsync(usage);
                await _db.SaveChangesAsync();

                return Json(new
                {
                    res = "ok",
                    data = response.jobUpdates.JobItems[0],
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        /// <summary>
        /// All open jobs
        /// </summary>
        /// <param name="customerNo"></param>
        /// <returns></returns>
        [HttpPost("JobsRequest")]
        public async Task<JsonResult> ManageCustJobsUpdateRequest(string customerNo)
        {
            try
            {
                var client = GetClient();
                var customerJobsUpdates = new Response1();
                var request = new ManageCustJobsUpdatesRequest(customerJobsUpdates, customerNo);
                var response = await client.ManageCustJobsUpdatesRequestAsync(request);
                if (response.return_value)
                {
                    return Json(new
                    {
                        res = "ok",
                        data = response.customerJobsUpdates.UpdateList,
                    });
                }

                return Json(new
                {
                    res = "err",
                    msg = response.customerJobsUpdates.ResponseMessage[0],
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// All open jobs which are still in progress or pending
        /// </summary>
        /// <param name="customerNo"></param>
        /// <returns></returns>
        [HttpPost("OpenJobsRequest")]
        public async Task<JsonResult> ManageCustOpenJobsRequest(string customerNo)
        {
            try
            {
                var client = GetClient();
                var customerOpenJobs = new Response2();
                var request = new ManageCustOpenJobsRequest(customerOpenJobs, customerNo);
                var response = await client.ManageCustOpenJobsRequestAsync(request);
                if (response.return_value)
                {
                    return Json(new
                    {
                        res = "ok",
                        data = response.customerOpenJobs.JobList,
                    });
                }

                return Json(new
                {
                    res = "err",
                    msg = response.customerOpenJobs.ResponseMessage[0],
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }


        [HttpPost("GetCustomerName")]
        public async Task<JsonResult> GetCustomerName(string customerNo, bool dataOnly = false)
        {
            try
            {
                var client = GetClient();
                var request = new GetCustomerName {customerNo = customerNo, result = ""};
                var response = await client.GetCustomerNameAsync(request);
                if (response.return_value)
                {
                    return dataOnly
                        ? Json(response.result)
                        : Json(new
                        {
                            res = "ok",
                            data = response.result,
                        });
                }

                return Json(new
                {
                    res = "err",
                    msg = response.result,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }
    }
}