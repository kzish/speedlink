﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Text;
using SpeedLinkApi.Models;
using Microsoft.EntityFrameworkCore;

namespace SpeedLinkApi.Controllers
{
    [Route("speedlink/v1/Auth")]
    [Authorize]
    public class AuthController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private dbContext db = new dbContext();

        public AuthController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        /// <summary>
        /// client using client id and client secret to authenticate and recieve access_token
        /// Add { Bearer <access_token> } in header request of each subsequent request
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="clientSecret"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("RequestToken")]
        public async Task<IActionResult> RequestToken(string clientID = "test_user", string clientSecret = "12345")
        {
            try
            {
                var identityServerEndPoint = Globals.auth_endpoint;

                var discoveryClient = new DiscoveryClient(identityServerEndPoint)
                {
                    Policy = {ValidateIssuerName = false, ValidateEndpoints = false}
                };

                // Accept the configuration even if the issuer and endpoints don't match

                var disco = await discoveryClient.GetAsync();
                var tokenClient = new TokenClient(
                    disco.TokenEndpoint,
                    clientID,
                    clientSecret);
                var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api");
                return tokenResponse.IsError ? Json(tokenResponse.Exception) : Json(tokenResponse.Json);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }


        /// <summary>
        /// authenticate the mobile app
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("MobileAuth")]
        public async Task<JsonResult> MobileAuth(string email = "test_client@rubiem.com", string password = "Rubiem#99")
        {
            try
            {
                var result = await signInManager.PasswordSignInAsync(email, password, false, false);
                if (result.Succeeded)
                {
                    var aspNetUser = db.AspNetUsers.Where(i => i.Email == email).Include(i => i.MClient)
                        .FirstOrDefault();

                    return Json(new
                    {
                        res = "ok",
                        data = aspNetUser?.MClient.CustomerNumber
                    });
                }
                else
                {
                    return Json(new
                    {
                        res = "err",
                        msg = "failed"
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }
    }
}