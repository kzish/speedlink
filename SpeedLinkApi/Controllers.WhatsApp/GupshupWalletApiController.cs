﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpeedLinkApi.Models;

namespace WalletApi.Controllers
{
    /// <summary>
    /// the telegram wallet api
    /// </summary>
    public class GupshupWalletApiController : Controller
    {

        //receive message callback
        [HttpPost("RecieveMessageCallBack")]
        public void RecieveMessageCallBack()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var message = new RecieveMessageCallBack();
                    //
                    string body = reader.ReadToEnd();

                    var log = new Logs();
                    log.Data1 = "Callback";
                    log.Data2 = body;
                    log.Date = DateTime.Now;
                    var db = new dbContext();
                    db.Logs.Add(log);
                    db.SaveChanges();
                    db.Dispose();

                    dynamic json_data = JsonConvert.DeserializeObject(body);
                    //
                    message.from = json_data.payload.source;
                    message.content = json_data.payload.payload.text;
                    WhatsAppWalletApiController.RecieveMessageCallBack(message);

                    //return Ok(DateTime.Now);
                }
            }
            catch(Exception ex)
            {
                var log = new Logs();
                log.Data1 = "error on callback";
                log.Data2 = ex.Message;
                log.Date = DateTime.Now;
                var db = new dbContext();
                db.Logs.Add(log);
                db.SaveChanges();
                db.Dispose();
            }
        }

        //send a text message
        public static async Task<bool> SendMessageText(string mobileNumber, string message_text)
        {
            try
            {

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                var string_content = new StringContent($"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={message_text}&src.name=RubiWallet"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content.ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs() { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    db.Logs.Add(log);
                    db.SaveChanges();
                    db.Dispose();
                }
                return false;
            }
        }

        //send text message with image
        public static async Task<bool> SendMessageImageAndText(string mobileNumber, string image_url, string caption)
        {
            try
            {

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Accept", "application/x-www-form-urlencoded");
                client.DefaultRequestHeaders.Add("apikey", $"{Globals.gupshup_apikey}");
                client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

                dynamic json = new JObject();
                json.type = "image";
                json.originalUrl = image_url;
                json.previewUrl = image_url;
                json.caption = caption;

                string json_message = JsonConvert.SerializeObject(json);
                var string_content = new StringContent($"channel=whatsapp&source={Globals.gupshup_source}&destination={mobileNumber}&message={json_message}"
                    , Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                var response = await client.PostAsync($"{Globals.gupshup_end_point}", string_content).Result.Content.ReadAsStringAsync();

                return true;
            }
            catch (Exception ex)
            {
                using (var db = new dbContext())
                {
                    var log = new Logs() { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    db.Logs.Add(log);
                    db.SaveChanges();
                    db.Dispose();
                }
                return false;
            }

        }

        //send an error text message
        public static async Task<bool> SendErrorMessageText(string mobileNumber, string message_text)
        {
            return await SendMessageText(mobileNumber, message_text);
        }

    }
}
