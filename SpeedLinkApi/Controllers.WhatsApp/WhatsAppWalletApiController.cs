﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using SpeedLinkApi.Menus.MainMenu;
using SpeedLinkApi.Models;

namespace WalletApi.Controllers
{
    /// <summary>
    /// this is a proxy class that will send messages on the selected platforms
    /// </summary>
    public class WhatsAppWalletApiController : Controller
    {
        //1=telegram
        //2=gupshup
        private static int platform = 2;
        private static string start_text = "#"; //start text

        //fetch the session data
        public static MSession GetSessionData(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var sessionData = db.MSession
                    .FirstOrDefault(i => i.WhatsappMobileNumber == message.from);

                if (sessionData != null) return sessionData;
                //if no session exists create new session and save it
                sessionData = new MSession();
                sessionData.WhatsappMobileNumber = message.from;
                sessionData.Date = DateTime.Now;
                sessionData.LastMenu = "";
                sessionData.NextCount = 0;
                db.MSession.Add(sessionData);
                db.SaveChanges();

                return sessionData;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }

        //clear my session to start afresh
        private static void ClearMySession(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var sessionData = db.MSession
                    .FirstOrDefault(i => i.WhatsappMobileNumber == message.from);

                if (sessionData != null)
                {
                    db.MSession.Remove(sessionData); //remove old session to clear session
                }

                //add new blank session
                var newSession = new MSession
                {
                    WhatsappMobileNumber = message.from, Date = DateTime.Now, LastMenu = "", NextCount = 0
                };
                db.MSession.Add(newSession);
                //
                db.SaveChanges();
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                db.Dispose();
            }
        }

        //receive message callback and executes the menu
        public static void RecieveMessageCallBack(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                //if client has no whatsapp number => return
                var isClientWhatsAppEnabled = db.MClient.Any(i => i.MobileWhatsapp == message.from);
                if (!isClientWhatsAppEnabled)
                {
                    SendMessageText(message.from, "You are not enabled for this service")
                        .Wait();
                    return;
                }

                //
                if (message.content.ToLower() == "##") ClearMySession(message); //will clear the entire session and start over

                var sessionData = GetSessionData(message);

                //first time
                if (string.IsNullOrEmpty(sessionData.CustomerNumber) && string.IsNullOrEmpty(sessionData.LastMenu))
                    MainMenu.Menu_Start(message); //prompt enter customer number
                //
                // else if (sessionData.LastMenu == "main" && !message.content.All(char.IsDigit) ||
                //          message.content == "#") //# = main menu
                //     MainMenu.Menu_0(message);

                //option 1 view all updates
                // else if (sessionData.LastMenu == "main" /*&& message.content == "1"*/)
                else MenuViewAllUpdates.Menu1(message);
                // else if (sessionData.LastMenu == "main.view_all_updates" &&
                // (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                // MenuViewAllUpdates.Menu1(message);

                //option 2 sub for all job updates
                // else if (sessionData.LastMenu == "main" && message.content == "2")
                //     MenuSubForALLJobUpdates.Menu1(message);
                // else if (sessionData.LastMenu == "main.prompt_sub_for_all_job_updates" &&
                //          message.content.All(char.IsDigit))
                //     MenuSubForALLJobUpdates.Menu2(message);
                //
                // //option 3 > cancel all update subs.
                // else if (sessionData.LastMenu == "main" && message.content == "3")
                //     MenuCancelAllJobUpdates.Menu1(message);
                // else if (sessionData.LastMenu == "main.prompt_cancel_all_job_subscriptions")
                //     MenuCancelAllJobUpdates.Menu2(message); //cancel all update subscriptions


                // else //show the last successful menu
                // SendMessageText(message.from,
                // $"Invalid response, try again or enter '{start_text}' to return to main menu").Wait();
            }
            catch (Exception ex)
            {
                SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageText(mobileNumber, message_text);
            }

            if (platform == 2)
            {
                message_text = message_text.Replace("&", "AND"); //whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageText(mobileNumber, message_text);
            }

            return new JsonResult(new { });
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content,
            string caption)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageImageAndText(mobileNumber, image_content, caption);
            }

            if (platform == 2)
            {
                caption = caption.Replace("&", "AND"); //whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageImageAndText(mobileNumber, image_content,
                    HttpUtility.HtmlEncode(caption));
            }

            return new JsonResult(new { });
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            //if (platform == 1) await TelegramWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //if (platform == 2) await GupshupWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //send errot through telegram always
            await TelegramWalletApiController.SendErrorMessageText("586097924", message_text);
            return new JsonResult(new { });
        }
    }
}