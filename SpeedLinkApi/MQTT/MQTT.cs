﻿using Newtonsoft.Json;
using SpeedLinkApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

public class MQTT
{
    public static MqttClient mqtt;

    public static void initMqttClient()
    {
        try
        {
            if (mqtt != null) return;//only init the instance once
            mqtt = new MqttClient(Globals.mqtt_server);
            mqtt.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            string clientId = "123Server" + DateTime.Now;//this is to make the connection unique
            mqtt.Connect(clientId);
            mqtt.Subscribe(new string[] { "admin_messages" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
        }
        catch (Exception ex)
        {
            using (var _db = new dbContext())
            {
                var log = new Logs() { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                _db.Logs.Add(log);
                _db.SaveChanges();
                _db.Dispose();
            }
        }
    }


    public static void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
    {
        try
        {
            //recieve message and route it to the appropriate controller
            //persist the message to the database
            //forward the message to the admin_vue_client
            var msg = Encoding.ASCII.GetString(e.Message);
            var message = JsonConvert.DeserializeObject<MChatMessage>(msg);
            var db = new dbContext();
            message.Date = DateTime.Now;
            db.MChatMessage.Add(message);
            db.SaveChanges();
            db.Dispose();

            var json = JsonConvert.SerializeObject(message);
            mqtt.Publish("admin_client", Encoding.ASCII.GetBytes(json), 1, false);
        }
        catch (Exception)
        {
            // ignored
        }
    }
}



