﻿using System;
using System.Collections.Generic;

namespace SpeedLinkApi.Models
{
    public partial class MWhatsappUsageStats
    {
        public int Id { get; set; }
        public string AspNetUserId { get; set; }
        public DateTime Date { get; set; }

        public virtual AspNetUsers AspNetUser { get; set; }
    }
}
