﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpeedLinkApi.Controllers;
using SpeedLinkApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WalletApi.Controllers;

namespace SpeedLinkApi.Menus.MainMenu
{
    public class MainMenu
    {
        private static int jobs_array_limit = 5;

        //menu_start
        public static void Menu_Start(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var sessionData = db.MSession.Find(message.from);
                //send first menu
                var menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_start.txt");
                var image = $"{Globals.base_path}/Raw/speedlink_logo.jpg";
                WhatsAppWalletApiController.SendMessageImageAndText(message.from, image, menu).Wait();
                //save session data
                sessionData.LastMenu = "main";
                sessionData.Date = DateTime.Now;
                sessionData.NextCount = 0;
                sessionData.PrevCount = 0;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle menu 0 events
        public static void Menu_0(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                if (string.IsNullOrEmpty(session_data.CustomerNumber))//no customer number exists for this user
                {
                    //fetch the customer name also confirms the customer number is valid
                    var customer_number = message.content;//speedlink customer number
                    dynamic confirm_customer_number = new ProcessController().GetCustomerName(customer_number).Result.Value;
                    if (confirm_customer_number.res == "ok")
                    {
                        //save the customer number it was correct
                        session_data.CustomerNumber = customer_number;
                        session_data.SpeedlinkName = confirm_customer_number.data;
                        menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_0.txt");//show the next menu
                        menu = menu.Replace("{{speedlink_user}}", session_data.SpeedlinkName);//replace the template with the user's name
                    }
                    else
                    {
                        //customer number was incorrect
                        menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", confirm_customer_number.msg);
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        //return to start menu
                        Menu_Start(message);
                        return;//exit method
                    }
                }
                else //customer number already exists
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_0.txt");//show the next menu
                    menu = menu.Replace("{{speedlink_user}}", session_data.SpeedlinkName);//replace the template with the user's name
                }

                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                session_data.LastMenu = "main";
                session_data.NextCount = 0;
                session_data.PrevCount = 0;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle menu 01 events
        public static void Menu_01(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                var current_page = 1;
                //list open jobs
                dynamic request = new ProcessController().ManageCustJobsUpdateRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    current_page = session_data.NextCount;
                    var jsonArray = json.Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "01";//current menu
                    menu = menu.Replace("{{page}}", $"{current_page + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "main";//go back
                }
                session_data.NextCount = 0;//init first page
                db.SaveChanges();
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle next menu 01N events, NEXT
        public static void Menu_01N(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {

                var session_data = db.MSession.Find(message.from);

                int pages = 0;
                //list open jobs
                dynamic request = new ProcessController().ManageCustJobsUpdateRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    if (session_data.NextCount < pages)
                    {
                        session_data.NextCount += 1;//advance 1
                    }

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "01";//current menu

                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "01";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle prev menu 01P events, PREV
        public static void Menu_01P(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {

                var session_data = db.MSession.Find(message.from);
                if (session_data.NextCount >= 1)
                {
                    session_data.NextCount -= 1;//reduce 1
                }
                int pages = 0;
                //var current_page = 1;
                //list open jobs
                dynamic request = new ProcessController().ManageCustJobsUpdateRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "01";//current menu

                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "01";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle menu 01_END events
        public static void Menu_01_END(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                var customerNo = session_data.CustomerNumber;
                var jobNo = message.content;

                dynamic request = new ProcessController().ManageJobStatusUpdateRequest(customerNo, jobNo).Result.Value;
                if (request.res == "ok")
                {
                    //important stuff
                    menu += $"🗓*StatusDate*: {request.data.UpdateList[0].StatusDate}{Environment.NewLine}";
                    menu += $"*ContainerNo*: {request.data.UpdateList[0].ContainerNo}{Environment.NewLine}";
                    menu += $"*Status*: {request.data.UpdateList[0].Status[0]}{Environment.NewLine}";
                    //menu+= $"lineNo: {request.data.UpdateList[0].LineNo}{Environment.NewLine}";
                    menu += $"{Environment.NewLine}{Environment.NewLine}";
                    menu += $"*JobNo*: {request.data.JobNo[0]}{Environment.NewLine}";
                    menu += $"*JobDescription*: {request.data.JobDescription[0]}{Environment.NewLine}";
                    menu += $"*Commodity*: {request.data.Commodity[0]}{Environment.NewLine}";
                    menu += $"*JobType*: {request.data.JobType[0]}{Environment.NewLine}";
                    menu += $"*Route*: {request.data.Route[0]}{Environment.NewLine}";
                    menu += $"*TransportMode*: {request.data.TransportMode[0]}{Environment.NewLine}";
                    menu += $"*CustomerOrderNo*: {request.data.CustomerOrderNo[0]}{Environment.NewLine}";
                    menu += $"*SupplierInvoiceNo*: {request.data.SupplierInvoiceNo[0]}{Environment.NewLine}";
                    menu += $"*BillOrManifestNo*: {request.data.BillOrManifestNo[0]}{Environment.NewLine}";
                    menu += $"*VolumetricWeight*: {request.data.VolumetricWeight[0]}{Environment.NewLine}";
                    menu += $"*ActualWeight*: {request.data.ActualWeight[0]}{Environment.NewLine}";
                    menu += $"*NoOfPeices*: {request.data.NoOfPeices[0]}{Environment.NewLine}";
                    menu += $"*PlaceOfReceipt*: {request.data.PlaceOfReceipt[0]}{Environment.NewLine}";
                    menu += $"*PortOfLoading*: {request.data.PortOfLoading[0]}{Environment.NewLine}";
                    menu += $"*PortOfDischarge*: {request.data.PortOfDischarge[0]}{Environment.NewLine}";
                    menu += $"*PlaceOfDelivery*: {request.data.PlaceOfDelivery[0]}{Environment.NewLine}";
                    menu += $"{Environment.NewLine}";
                    session_data.LastMenu = "main";
                    session_data.Date = DateTime.Now;
                    session_data.NextCount = 0;
                    session_data.PrevCount = 0;
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                }
                db.SaveChanges();
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                //send main menu
                message.content = "#";
                Menu_0(message);
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }



        //handle menu 02 events
        public static void Menu_02(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                var current_page = 1;
                //list open jobs still pending or in progress
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    current_page = session_data.NextCount;
                    var jsonArray = json.Take(jobs_array_limit);
                    int index = 0;
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "02";//current menu
                    menu = menu.Replace("{{page}}", $"{current_page + 1} of {pages}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "main";//go back
                }
                session_data.NextCount = 0;//init first page
                db.SaveChanges();
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle next menu 02N events, NEXT
        public static void Menu_02N(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                //list open jobs
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    if (session_data.NextCount < pages)
                    {
                        session_data.NextCount += 1;//advance 1
                    }

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "02";//current menu
                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "02";//go back
                }
                db.SaveChanges();
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle prev menu 02P events, PREV
        public static void Menu_02P(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                if (session_data.NextCount >= 1)
                {
                    session_data.NextCount -= 1;//reduce 1
                }
                int pages = 0;
                //var current_page = 1;
                //list open jobs
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_01.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "02";//current menu

                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "02";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle menu 02_END events
        public static void Menu_02_END(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                var customerNo = session_data.CustomerNumber;
                var jobNo = message.content;

                dynamic request = new ProcessController().ManageJobStatusUpdateRequest(customerNo, jobNo).Result.Value;
                if (request.res == "ok")
                {

                    //important stuff
                    menu += $"🗓*StatusDate*: {request.data.UpdateList[0].StatusDate}{Environment.NewLine}";
                    menu += $"*ContainerNo*: {request.data.UpdateList[0].ContainerNo}{Environment.NewLine}";
                    menu += $"*Status*: {request.data.UpdateList[0].Status[0]}{Environment.NewLine}";
                    menu += $"{Environment.NewLine}{Environment.NewLine}";
                    menu += $"*JobNo*: {request.data.JobNo[0]}{Environment.NewLine}";
                    menu += $"*JobDescription*: {request.data.JobDescription[0]}{Environment.NewLine}";
                    menu += $"*Commodity*: {request.data.Commodity[0]}{Environment.NewLine}";
                    menu += $"*JobType*: {request.data.JobType[0]}{Environment.NewLine}";
                    menu += $"*Route*: {request.data.Route[0]}{Environment.NewLine}";
                    menu += $"*TransportMode*: {request.data.TransportMode[0]}{Environment.NewLine}";
                    menu += $"*CustomerOrderNo*: {request.data.CustomerOrderNo[0]}{Environment.NewLine}";
                    menu += $"*SupplierInvoiceNo*: {request.data.SupplierInvoiceNo[0]}{Environment.NewLine}";
                    menu += $"*BillOrManifestNo*: {request.data.BillOrManifestNo[0]}{Environment.NewLine}";
                    menu += $"*VolumetricWeight*: {request.data.VolumetricWeight[0]}{Environment.NewLine}";
                    menu += $"*ActualWeight*: {request.data.ActualWeight[0]}{Environment.NewLine}";
                    menu += $"*NoOfPeices*: {request.data.NoOfPeices[0]}{Environment.NewLine}";
                    menu += $"*PlaceOfReceipt*: {request.data.PlaceOfReceipt[0]}{Environment.NewLine}";
                    menu += $"*PortOfLoading*: {request.data.PortOfLoading[0]}{Environment.NewLine}";
                    menu += $"*PortOfDischarge*: {request.data.PortOfDischarge[0]}{Environment.NewLine}";
                    menu += $"*PlaceOfDelivery*: {request.data.PlaceOfDelivery[0]}{Environment.NewLine}";
                    menu += $"{Environment.NewLine}";
                    //ressetting the menu
                    session_data.LastMenu = "main";
                    session_data.Date = DateTime.Now;
                    session_data.NextCount = 0;
                    session_data.PrevCount = 0;
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
                //send main menu
                message.content = "#";
                Menu_0(message);

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }



        //handle menu 03 events
        public static void Menu_03(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                var current_page = 1;
                //list open jobs still pending or in progress
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_03.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    current_page = session_data.NextCount;
                    var jsonArray = json.Take(jobs_array_limit);
                    int index = 0;
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "03";//current menu
                    menu = menu.Replace("{{page}}", $"{current_page + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "main";//go back
                }
                session_data.NextCount = 0;//init first page
                db.SaveChanges();
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle next menu 03N events, NEXT
        public static void Menu_03N(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                //list open jobs
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_03.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;
                    if (session_data.NextCount < pages)
                    {
                        session_data.NextCount += 1;//advance 1
                    }

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "03";//current menu

                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "03";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle prev menu 03P events, PREV
        public static void Menu_03P(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                var session_data = db.MSession.Find(message.from);
                if (session_data.NextCount >= 1)
                {
                    session_data.NextCount -= 1;//reduce 1
                }
                int pages = 0;
                //var current_page = 1;
                //list open jobs
                dynamic request = new ProcessController().ManageCustOpenJobsRequest(session_data.CustomerNumber).Result.Value;
                if (request.res == "ok")
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_03.txt");
                    string job_list = string.Empty;
                    var a = JsonConvert.SerializeObject(request.data);
                    JArray json = JArray.Parse(a);//array of jobs
                    pages = json.Count() / jobs_array_limit;

                    var jsonArray = json.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (dynamic item in jsonArray)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNo}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "03";//current menu
                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", request.msg);
                    session_data.LastMenu = "03";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //handle menu 03_END events
        public static void Menu_03_END(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //subscribe for an update
                var session_data = db.MSession.Find(message.from);
                var customer_number = message.content;//speedlink customer number
                var subscription = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from && i.JobNumber == message.content).FirstOrDefault();
                //subscription exists in the db already
                if (subscription != null)
                {
                    menu = $"✅✅You have already subscribed to Job Updates for{Environment.NewLine}";
                    menu += $"*{subscription.JobNumber}*{Environment.NewLine}";
                    menu += $"_{subscription.JobDescription}_";
                    //resset the session
                    session_data.LastMenu = "main";
                    session_data.NextCount = 0;
                    session_data.PrevCount = 0;
                }
                else
                {
                    //check this job exists at speedlink endpoint
                    var client = new HttpClient();
                    //get updates
                    dynamic response_job = new ProcessController().ManageJobStatusUpdateRequest(session_data.CustomerNumber, message.content).Result.Value;
                    //job exists
                    if (response_job.res == "ok")
                    {
                        var jobNo = response_job.data.JobNo[0];
                        var jobDescription = response_job.data.JobDescription[0];

                        var whats_app_client = db.MClient.Where(i => i.MobileWhatsapp == message.from).FirstOrDefault();
                        if (whats_app_client == null)
                        {
                            menu = "❌You are not registered for this whatapp service.";
                        }
                        else
                        {
                            var new_subscription = new MSubscriptions();
                            new_subscription.AspNetUserId = whats_app_client.Id;
                            new_subscription.WhatsappMobileNumber = message.from;
                            new_subscription.JobNumber = jobNo;
                            new_subscription.CustomerNumber = whats_app_client.CustomerNumber; ;
                            new_subscription.JobDescription = jobDescription;
                            db.MSubscriptions.Add(new_subscription);
                            menu = $"✅✅You have subscribed to{Environment.NewLine}";
                            menu += $"*{jobNo}*{Environment.NewLine}";
                            menu += $"_{jobDescription}_";
                            menu += $"{Environment.NewLine}";
                        }

                    }
                    //job does not exist
                    else
                    {
                        menu = $"❌Job with JobNumber {message.content} does not exist.{Environment.NewLine}Try again.";
                        session_data.LastMenu = "03";//error or invalid input stay on this menu
                    }
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
                //send main menu
                message.content = "#";
                Menu_0(message);
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //handle menu 04 events
        public static void Menu_04(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //confirm cancel one subcriptions
                var session_data = db.MSession.Find(message.from);
                session_data.LastMenu = "04";
                db.SaveChanges();
                menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_04.txt");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
        }

        //handle menu 04_END events
        public static void Menu_04_END(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //cancel an update subscription

                var session_data = db.MSession.Find(message.from);
                var customer_number = message.content;//speedlink customer number
                var subscription = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from && i.JobNumber == message.content).FirstOrDefault();
                if (subscription != null)
                {
                    menu = $"✅✅You have successfully de-registered updates for *{subscription.JobNumber}*";
                    db.Remove(subscription);//remove the sub
                    //resset the session
                    session_data.LastMenu = "main";
                    session_data.NextCount = 0;
                    session_data.PrevCount = 0;
                }
                else
                {
                    menu = $"❌You dont have a subscription with JobNumber {message.content}.{Environment.NewLine}";
                    session_data.LastMenu = "04";//error or invalid input stay on this menu
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
                //send main menu
                message.content = "#";
                Menu_0(message);
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }




       




        //handle menu 06 events
        public static void Menu_06(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //view all my subscriptions

                var session_data = db.MSession.Find(message.from);
                int pages = 0;
                var current_page = 1;
                //list open jobs still pending or in progress
                var subscriptions = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from).ToList();
                if (subscriptions.Count > 0)
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_06.txt");
                    string job_list = string.Empty;
                    pages = subscriptions.Count() / jobs_array_limit;
                    current_page = session_data.NextCount;
                    var sub_array = subscriptions.Take(jobs_array_limit);
                    int index = 0;
                    foreach (var item in sub_array)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNumber}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "06";//current menu
                    menu = menu.Replace("{{page}}", $"{current_page + 1} of {pages+1}");
                    //send
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    session_data.NextCount = 0;//init the first page
                    db.SaveChanges();
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "You have no subscriptions");
                    menu = menu.Replace("*Try again.*", "");
                    //send
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    session_data.NextCount = 0;//init the first page
                    session_data.LastMenu = "main";//go back
                    message.content = "#";
                    db.SaveChanges();
                    Menu_0(message);
                }
                
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle next menu 06N events, NEXT
        public static void Menu_06N(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {

                var session_data = db.MSession.Find(message.from);
                var subscriptions = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from).ToList();
                int pages = 0;
                //list my subscriptions
                if (subscriptions.Count > 0)
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_06.txt");
                    string job_list = string.Empty;

                    pages = subscriptions.Count() / jobs_array_limit;
                    if (session_data.NextCount < pages)
                    {
                        session_data.NextCount += 1;//advance 1
                    }
                    var sub_array = subscriptions.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (var item in sub_array)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNumber}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "06";//current menu
                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "You have no subscriptions");
                    session_data.LastMenu = "06";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //handle prev menu 06P events, PREV
        public static void Menu_06P(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {

                var session_data = db.MSession.Find(message.from);
                if (session_data.NextCount >= 1)
                {
                    session_data.NextCount -= 1;//reduce 1
                }
                int pages = 0;
                //list my subscriptions
                var subscriptions = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from).ToList();
                if (subscriptions.Count > 0)
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_06.txt");
                    string job_list = string.Empty;
                    pages = subscriptions.Count() / jobs_array_limit;

                    var sub_array = subscriptions.Skip(jobs_array_limit * session_data.NextCount).Take(jobs_array_limit);
                    int index = (jobs_array_limit * session_data.NextCount);
                    foreach (var item in sub_array)
                    {
                        index++;
                        job_list += $"{index} - *{item.JobNumber}*{Environment.NewLine}";
                        job_list += $"{item.JobDescription}{Environment.NewLine}{Environment.NewLine}";
                    }
                    menu = menu.Replace("{{job_list}}", job_list);
                    session_data.LastMenu = "06";//current menu

                    menu = menu.Replace("{{page}}", $"{session_data.NextCount + 1} of {pages+1}");
                }
                else
                {
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "You have no subscriptions");
                    session_data.LastMenu = "06";//go back
                }
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }



    }
}
