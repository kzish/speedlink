﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpeedLinkApi.Controllers;
using SpeedLinkApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WalletApi.Controllers;

namespace SpeedLinkApi.Menus.MainMenu
{
    public class MenuCancelAllJobUpdates
    {
        private static int jobs_array_limit = 5;
        
        //prompt cancel all job updates and confirm
        public static void Menu1(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //confirm cancel all subcriptions
                var session_data = db.MSession.Find(message.from);
                session_data.LastMenu = "main.prompt_cancel_all_job_subscriptions";
                db.SaveChanges();
                menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/menu_05.txt");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //accept confirmation and cancel updates
        public static void Menu2(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                int selection = int.Parse(message.content);
                if (selection == 1)
                {

                    //cancel all subcriptions and end the session
                    var my_subs = db.MSubscriptions.Where(i => i.WhatsappMobileNumber == message.from).ToList();
                    db.RemoveRange(my_subs);
                    var session_data = db.MSession.Find(message.from);
                    session_data.LastMenu = "main";
                    session_data.NextCount = 0;
                    session_data.PrevCount = 0;
                    db.SaveChanges();
                    menu = $"✅✅You have terminated all update subscriptions{Environment.NewLine}";
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //send main menu
                    message.content = "#";
                    MainMenu.Menu_0(message);
                }
                else
                {
                    //error occured
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid input, selection must be 1 to confirm.");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    Menu1(message);
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }


    }
}
