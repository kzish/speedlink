﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpeedLinkApi.Controllers;
using SpeedLinkApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WalletApi.Controllers;

namespace SpeedLinkApi.Menus.MainMenu
{
    public class MenuSubForALLJobUpdates
    {
        private static int jobs_array_limit = 5;

        //prompt select confirm to subscribe to all job updates
        public static void Menu1(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MSession.Find(message.from);
                //send first menu
                var menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/MenuSubForALLJobUpdates/menu_1.txt");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                //save session data
                session_data.LastMenu = "main.prompt_sub_for_all_job_updates";
                session_data.Date = DateTime.Now;
                session_data.NextCount = 0;
                session_data.PrevCount = 0;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //accept confirmation and save all the subscriptions
        public static void Menu2(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                int selection = int.Parse(message.content);
                if (selection == 1)
                {
                    //proceed to save these subscriptions
                    var mclient = db.MClient.Where(i => i.MobileWhatsapp == message.from).FirstOrDefault();
                    //get updates
                    dynamic jobs = new ProcessController().ManageCustJobsUpdateRequest(mclient.CustomerNumber).Result.Value;
                    //jobs exists
                    if (jobs.res == "ok")
                    {
                        foreach(var job in jobs.data)
                        {
                            string job_number = job.JobNo.ToString();
                            string job_description = job.JobDescription.ToString();
                            var subscription_exists = db.MSubscriptions
                                .Where(i => i.WhatsappMobileNumber == message.from)//mine
                                .Where(i => i.JobNumber == job_number)//this job number
                                .Any();
                            if (subscription_exists) continue;//skip this, dont add the same record twice
                            var new_subscription = new MSubscriptions();
                            new_subscription.AspNetUserId = mclient.Id;
                            new_subscription.WhatsappMobileNumber = message.from;
                            new_subscription.JobNumber = job_number;
                            new_subscription.CustomerNumber = mclient.CustomerNumber; ;
                            new_subscription.JobDescription = job_description;
                            db.MSubscriptions.Add(new_subscription);
                        }
                        db.SaveChanges();

                        //send
                        WhatsAppWalletApiController.SendMessageText(message.from, "*✅You have subscribed for all job updates*").Wait();
                        //save session
                        var session_data = db.MSession.Find(message.from);
                        session_data.LastMenu = "main";
                        db.SaveChanges();
                        //go to main menu
                        MainMenu.Menu_0(message);
                    }

                    else
                    {
                        //error occured
                        menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                        menu = menu.Replace("{{error_message}}", "Network error");
                        WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                        //prev menu
                        Menu1(message);
                    }


                }
                else
                {
                    //customer number was incorrect
                    menu = System.IO.File.ReadAllText(Globals.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", "Invalid selection must be 1 to confirm.");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    Menu1(message);
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
                var log = new Logs();
                log.Data1 = ex.Message;
                log.Data2 = ex.StackTrace;
                log.Date = DateTime.Now;
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

    }
}
