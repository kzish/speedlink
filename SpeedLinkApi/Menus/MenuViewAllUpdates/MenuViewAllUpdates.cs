﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SpeedLinkApi.Controllers;
using SpeedLinkApi.Models;
using WalletApi.Controllers;
using WS;

namespace SpeedLinkApi.Menus.MainMenu
{
    public class MenuViewAllUpdates
    {
        private static int array_limit = 5;

        //get all jobs and job description
        public static Dictionary<string, string> GetAllJobsAndJobDescription(string customer_number)
        {
            var dict = new Dictionary<string, string>();
            try
            {
                //open jobs
                dynamic jobs = new ProcessController().ManageCustOpenJobsRequest(customer_number).Result.Value;
                if (jobs.res == "ok")
                {
                    foreach (var job in jobs.data)
                    {
                        string jobNumber = job.JobNo.ToString();
                        string jobDescription = job.JobDescription.ToString();
                        dict.Add(jobNumber, jobDescription);
                    }
                }
            }
            catch (Exception ex)
            {
                using (var _db = new dbContext())
                {
                    var log = new Logs { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    _db.Logs.Add(log);
                    _db.SaveChanges();
                    _db.Dispose();
                }
            }
            return dict;
        }

        //Get All Job Statuses
        public static Dictionary<string, List<string>> GetAllJobStatuses(string customer_number, Dictionary<string, string> jobs)
        {
            var dict = new Dictionary<string, List<string>>();
            try
            {
                if (jobs.Count == 0) return dict;
                foreach (var job in jobs.Keys)
                {
                    dynamic update = new ProcessController()
                        .ManageJobStatusUpdateRequest(customer_number, job).Result.Value;

                    if (update.res == "ok")
                    {
                        string status_update = update.data.UpdateList[0].Status[0];
                        string customer_order_number = update.data.CustomerOrderNo[0];
                        string suplier_invoice_number = update.data.SupplierInvoiceNo[0];
                        string date = update.data.UpdateList[0].StatusDate;
                        string supplier_name = update.data.SupplierName[0];
                        var list = new List<string>
                        {
                             DateTime.Parse(date.Trim()).ToString("d-MMM-yyyy")
                            ,customer_order_number.Trim()
                            ,suplier_invoice_number.Trim()
                            ,status_update.Trim()
                            ,job.Trim()//job number
                            ,jobs[job].Trim()//job description
                            ,supplier_name

                        };
                        dict.Add(job, list);
                    }
                }
            }
            catch (Exception ex)
            {
                using (var _db = new dbContext())
                {
                    var log = new Logs { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                    _db.Logs.Add(log);
                    _db.SaveChanges();
                    _db.Dispose();
                }
            }
            return dict;
        }


        //navigate all updates
        public static void Menu1(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            WhatsAppWalletApiController.SendMessageText(message.from, "⏱ Processing Please wait...").Wait();
            try
            {
                var client = db.MClient.FirstOrDefault(i => i.MobileWhatsapp == message.from);
                var allJobs = GetAllJobsAndJobDescription(client?.CustomerNumber);

                //jobs timeout
                if (!allJobs.Any())
                {
                    WhatsAppWalletApiController.SendMessageText(message.from, "❌❌ Network timed out fetching jobs").Wait();
                    return;
                }

                var sessionData = db.MSession.Find(message.from);

                //number of pages
                var _pages = allJobs.Count / (decimal)array_limit;
                var pages = 0;
                if (_pages <= 1) pages = 1;
                else if (_pages > 1) pages = (int)_pages + 1;


                if (message.content.ToLower() == "n" && ((sessionData.NextCount + 1) < pages)) sessionData.NextCount++;//go next
                //else if (message.content.ToLower() == "n") return;
                if (message.content.ToLower() == "p" && (sessionData.NextCount >= 1)) sessionData.NextCount--;//go prev
                //else if (message.content.ToLower() == "p") return;

                //pagination
                var subList = allJobs
                    .Skip(array_limit * sessionData.NextCount)
                    .Take(array_limit)
                    .ToDictionary(x => x.Key, x => x.Value);

                var updates = GetAllJobStatuses(client.CustomerNumber, subList);

                //updates timeout
                if (updates.Count == 0)
                {
                    WhatsAppWalletApiController.SendMessageText(message.from, "❌❌ Network timed out fetching updates").Wait();
                    return;
                }

                var menu = File.ReadAllText(Globals.host.WebRootPath + "/MenuViewAllUpdates/menu_1.txt");

                var list = string.Empty;
                var index = array_limit * sessionData.NextCount;//start here
                
                menu = menu.Replace("{{name}}", new ProcessController().GetCustomerName(client.CustomerNumber, true).Result.Value.ToString());

                foreach (var (key, _) in updates)
                {
                    //  date
                    //,customer_order_number
                    //,suplier_invoice_number
                    //,status_update
                    //job number
                    //job description
                    index++;
                    list += $"*({index}) {updates[key][0]}*{Environment.NewLine}";//date
                    list += $"*JobNo*...{updates[key][4]}{Environment.NewLine}";//job number
                    list += $"*Job Desc*...{updates[key][5]}{Environment.NewLine}";//job description
                    list += $"*Cust. Order No*...{updates[key][1]}{Environment.NewLine}";//customer_order_number
                    list += $"*Supplier Inv. No*...{updates[key][2]}{Environment.NewLine}";//suplier_invoice_number
                    list += $"*Supplier Name*...{updates[key][6]}{Environment.NewLine}";//suplier_invoice_number
                    list += $"_{updates[key][3]}_{Environment.NewLine}{Environment.NewLine}";//status_update
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{(sessionData.NextCount + 1)} of {pages}");
                WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();

                sessionData.Date = DateTime.Now;
                // session_data.LastMenu = "main.view_all_updates";
                db.SaveChanges();//update session
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
                var log = new Logs { Data1 = ex.Message, Data2 = ex.StackTrace, Date = DateTime.Now };
                db.Logs.Add(log);
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

    }
}
