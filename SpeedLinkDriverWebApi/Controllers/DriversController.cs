﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using SpeedLinkDriverWebApi.Models;
using SpeedLinkDriverWebApi.WS;

namespace SpeedLinkDriverWebApi.Controllers
{
    [System.Web.Http.RoutePrefix("speedlink/v1")]
    public class DriversController : ApiController
    {
        /// <summary>
        /// Return the soap client
        /// </summary>
        /// <returns />
        private static DriverAppAPI_PortClient GetClient()
        {
            //configure binding
            var myBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic; //basic auth required

            //configure end point
            var endPoint =
                new EndpointAddress("http://196.44.191.67:7047/AppIntegration/WS/DriverAppTest/Codeunit/DriverAppAPI");

            //declare client
            var client = new DriverAppAPI_PortClient(myBinding, endPoint);

            //client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("WEBUSER", "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=");
            //for baic

            if (client.ClientCredentials == null) return client;

            client.ClientCredentials.UserName.UserName = "Webuser";
            client.ClientCredentials.UserName.Password = "XYlPy4s+XubkkZXeL+6OtMmEDSw2GdiBSEXthCjB0v8=";

            return client;
        }

        [System.Web.Http.Route("DriverLoginRequest")]
        public async Task<JsonResult> DriverLoginRequest(string driver_username, string driver_password)
        {
            try
            {
                return SuccessResult(
                    await GetClient().LoginRequestAsync(
                        new LoginRequest(
                            driver_username,
                            driver_password,
                            "",
                            ""
                        )
                    )
                );
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("DriverShiftsSummaryRequest"), System.Web.Http.HttpGet]
        public async Task<JsonResult> DriverShiftsSummaryRequest(string driver_username, string driver_session_id,
            bool extries_exist)
        {
            try
            {
                var wsClient = GetClient();
                var request = new ShiftsSummaryRequest(
                    driver_username,
                    driver_session_id,
                    "",
                    extries_exist,
                    new DeliveryShiftRoot()
                );
                return SuccessResult(await wsClient.ShiftsSummaryRequestAsync(request));
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("ShiftDetailRequest"), System.Web.Http.HttpGet]
        public async Task<JsonResult> ShiftDetailRequest(string username, string sessionId, string shiftNo)
        {
            try
            {
                return SuccessResult(await GetClient().ShiftDetailRequestAsync(new ShiftDetailRequest(
                    username,
                    sessionId,
                    shiftNo,
                    "",
                    new DeliveryShiftDetailRoot()
                )));
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [System.Web.Http.Route("DriverActionShiftRequest"), System.Web.Http.HttpGet]
        public async Task<JsonResult> DriverActionShiftRequest(string driver_username, string driver_session_id,
            string shift_no, int driver_action, string driver_comment = "")
        {
            try
            {
                var wsClient = GetClient();
                var request = new ActionShiftRequest(
                    driver_username,
                    driver_session_id,
                    shift_no,
                    driver_action,
                    driver_comment,
                    ""
                );
                var response = await wsClient.ActionShiftRequestAsync(request);
                return new JsonResult
                {
                    Data = new
                    {
                        res = "ok",
                        data = response
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("PODDetailRequest"), System.Web.Http.HttpGet]
        public async Task<JsonResult> PodDetailRequest(string driver_username, string driver_session_id,
            string shift_number, string pod_number)
        {
            try
            {
                var wsClient = GetClient();
                var request = new PODDetailRequest(
                    driver_username,
                    driver_session_id,
                    shift_number,
                    pod_number,
                    "",
                    new PODRoot()
                );
                var response = await wsClient.PODDetailRequestAsync(request);
                return new JsonResult
                {
                    Data = new
                    {
                        res = "ok",
                        data = response
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("PODUpdateRequest"), System.Web.Http.HttpPost]
        public async Task<JsonResult> PodUpdateRequest(string driverUsername, string driverSessionId,
            [Bind] PODUpdateRoot podUpdateDetail)
        {
            try
            {
                return SuccessResult(
                    await GetClient().PODUpdateRequestAsync(
                        new PODUpdateRequest(
                            driverUsername,
                            driverSessionId,
                            "",
                            podUpdateDetail
                        )));
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("RegisterDeviceRequest"), System.Web.Http.HttpPost]
        public async Task<JsonResult> RegisterDevice([FromBody] DeviceRegistration registration)
        {
            try
            {
                return SuccessResult(await GetClient()
                        .RegisterOrDeregisterDeviceAsync(
                            registration.SetType(DeviceRegistration.RegisterRequest).ToJson()
                        ));
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [System.Web.Http.Route("DeregisterDeviceRequest"), System.Web.Http.HttpPost]
        public async Task<JsonResult> DeregisterDevice([FromBody] DeviceRegistration registration)
        {
            try
            {
                return SuccessResult(
                    await GetClient().RegisterOrDeregisterDeviceAsync(
                        registration.SetType(DeviceRegistration.DeregisterRequest).ToJson()
                    )
                );
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        private static JsonResult ErrorResult(Exception ex) =>
            new JsonResult
            {
                Data = new
                {
                    res = "err",
                    msg = ex.Message
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        private static JsonResult SuccessResult<T>(T response) =>
            new JsonResult
            {
                Data = new
                {
                    res = "ok",
                    data = response
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
    }
}