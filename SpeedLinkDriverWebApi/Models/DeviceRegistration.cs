using Newtonsoft.Json;

namespace SpeedLinkDriverWebApi.Models
{
    public class DeviceRegistration
    {
        public static readonly string RegisterRequest = "RegisterRequest";
        public static readonly string DeregisterRequest = "DeregisterRequest";

        public DeviceRegistration SetType(string type)
        {
            RequestType = type;
            return this;
        }

        public string ToJson() => JsonConvert.SerializeObject(this);

        public string RequestType { get; set; } = RegisterRequest;
        public string RegType { get; set; } = "Driver";
        public string DeviceID { get; set; }
        public string OwnerNo { get; set; }
        public string MobileNo { get; set; }
    }
}